package main

import (
	"fmt"
	"runtime"
	"sync"
)

// Есть двумерный слайс из строк. Надо в каждом из столбцов найти дубли и вывести их индексы(в формате {столбец, строка}).
// Т.е. для 0-го столбца должно быть {0, 2} и {0, 3}, для 1-го {1, 0}, {1, 1},  {1, 4}, {1, 2}, {1, 3}
//
//
// arr := [][]string{
// {"1014065", "1", "4708", "4709"},
// {"1014071", "1", "4708", "4697"},
// {"1014130", "2", "9416", "8004"},
// {"1014130", "2", "9416", "1412"},
// {"1014238", "1", "4708", "4694"},
// {"1014240", "3", "14124", "14090"},
// }

func main() {
	arr := [][]string{
		{"1014065", "1", "4708", "4709"},
		{"1014071", "1", "4708", "4697"},
		{"1014130", "2", "9416", "8004"},
		{"1014130", "2", "9416", "1412"},
		{"1014238", "1", "4708", "4694"},
		{"1014240", "3", "14124", "14090"},
	}

	fmt.Println(findDoubles(arr))
}

type doubleInfo struct {
	rowIndex int
	isAdd    bool
}

func findDoubles(arr [][]string) [][]int {
	// ограничиваем количество goroutine
	maxGoroutine := runtime.NumCPU()
	sem := make(chan struct{}, maxGoroutine)
	defer close(sem)
	// предполагаем, что длина всех строк одинакова
	numRows := len(arr)
	numColumns := len(arr[0])
	var resDoubles [][]int
	var wg sync.WaitGroup
	var mu sync.Mutex

	// Проходимся по колоннам
	for i := 0; i < numColumns; i++ {
		wg.Add(1)
		sem <- struct{}{}
		go func(c int) {
			var doubles [][]int
			defer wg.Done()
			defer func() { <-sem }()
			// doubleInfo нужно, чтобы предотвращать повторное добавление одного и того же индекса (isAdd)
			store := make(map[string]*doubleInfo, numRows)
			// идем по всем значениям в колонне и ищем дубликаты
			for r := 0; r < numRows; r++ {
				v := arr[r][c]
				info, ok := store[v]
				if !ok {
					store[v] = &doubleInfo{rowIndex: r, isAdd: false}
					continue
				}

				// Если найден дубликат, то проверяем был ли добавлен родительский индекс
				if !info.isAdd {
					store[v].isAdd = true
					doubles = append(doubles, []int{c, info.rowIndex}, []int{c, r})
				} else {
					doubles = append(doubles, []int{c, r})
				}
			}

			if len(doubles) != 0 {
				mu.Lock()
				resDoubles = append(resDoubles, doubles...)
				mu.Unlock()
			}
		}(i)
	}

	wg.Wait()
	return resDoubles
}
